﻿using UnityEngine;
using System.Collections;

public class MoveBackAndForth : Rewindable {
	public float x1;
	public float y1;
	public float x2;
	public float y2;
	public int frames;

	private bool moving;
	private bool movingRight;
	private int atFrame;
	private bool savedMoving;
	private int savedFrame;
	private bool savedMovingRight;
	private float xDistPerFrame;
	private float yDistPerFrame;
	
	protected override void Start(){
		moving = true;
		movingRight = true;
		atFrame = 0;
		xDistPerFrame = (x2 - x1) / frames;
		yDistPerFrame = (y2 - y1) / frames;
		base.Start ();
	}

	void Update(){
		if (moving) {
			transform.position = new Vector2(x1 + xDistPerFrame * atFrame, y1 + yDistPerFrame * atFrame);
			if(movingRight){
				atFrame++;
			}else{
				atFrame--;
			}
			if(atFrame > frames){
				movingRight = false;
				atFrame -= 2;
			}else if(atFrame < 0){
				movingRight = true;
				atFrame += 2;
			}
		}
	}
	
	public override void DisableBehavior(){
		moving = false;
	}

	public override void EnableBehavior(){
		moving = true;
	}
	
	protected override void SaveOriginalState(){
		savedFrame = atFrame;
		savedMovingRight = movingRight;
		savedMoving = moving;
		base.SaveOriginalState ();
	}
	
	protected override void LoadOriginalState(){
		atFrame = savedFrame;
		movingRight = savedMovingRight;
		moving = savedMoving;
		base.LoadOriginalState ();
	}

}
