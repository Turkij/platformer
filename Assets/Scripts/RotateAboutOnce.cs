﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RotateAboutOnce : Reactor {
	public float x;
	public float y;
	public float degrees;
	public int frames;
	
	private Vector3 startPos;
	private Quaternion startRotation;
	private Coroutine runningCoroutine;
	private int savedFrame;
	private int lastFrame;
	private float degreesPerFrame;
	private bool done;
	private bool savedDone;
	private List<Quaternion> rotations;
	
	protected override void Start(){
		degreesPerFrame = degrees / frames;
		startRotation = transform.rotation;
		startPos = transform.position;
		done = false;
		rotations = new List<Quaternion> ();
		lastFrame = -1;
		base.Start ();
	}
	
	public override void DisableBehavior(){
		if (runningCoroutine != null) {
			StopCoroutine (runningCoroutine);
		}
		base.DisableBehavior();
	}
	
	protected override void SaveOriginalState(){
		savedDone = done;
		savedFrame = lastFrame;
		base.SaveOriginalState ();
	}
	
	protected override void LoadOriginalState(){//called twice, second with savedFrame in error sometimes
		done = savedDone;
		print (savedFrame);
		if (savedFrame != -1) {
			runningCoroutine =  StartCoroutine (ReactHelper (savedFrame));
			lastFrame = savedFrame;
		}
		base.LoadOriginalState ();
	}
	
	protected override void RecordState(){
		rotations.Add (transform.rotation);
		base.RecordState ();
	}
	
	protected override void LoadRecordedState(int index){
		transform.rotation = rotations [index];
		base.LoadRecordedState (index);
	}
	
	public override void ClearRecordings(){
		rotations.Clear ();
		base.ClearRecordings ();
	}
	
	public override void React ()
	{
		if (!done) {
			runningCoroutine =  StartCoroutine (ReactHelper (1));
			done = true;
		}
	}
	
	private IEnumerator ReactHelper(int start){
		for (int i=start; i<=frames; i++) {
			lastFrame = i;
			transform.rotation = startRotation;
			transform.position = startPos;
			transform.RotateAround(new Vector3(x, y, 0), Vector3.forward, degreesPerFrame*i);
			yield return null;
		}
		lastFrame = -1;
	}
}
