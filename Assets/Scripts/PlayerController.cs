﻿using UnityEngine;
using System.Collections;

public class PlayerController : Rewindable {

	public const float INTERACT_DISTANCE = 1f;

	private Animator animator;
	private Rigidbody2D rigidBody;
	private ShadowPath shadowPath;
	public GameObject lastMarker;
	private bool recording;
	private ShadowPath.SpecialAction currentSpecialAction;
	public bool isControllable;
	public const float MAX_STANDING_SPEED = 0.2f;

	public float speed;
	public float airSpeed;
	public float accel;
	public float airAccel;
	public float deccel;
	public float jumpStrength;
	public float wallJumpStrength_horizontal;
	public float wallJumpStrength_vertical;
	public int maxShadowsOut;

	void Awake(){
		animator = GetComponent<Animator> ();
		rigidBody = GetComponent<Rigidbody2D> ();
		isControllable = true;
		speed = 8;
		airSpeed = 2;
		accel = 1.2f;
		airAccel = 0.08f;
		deccel = 0.8f;
		jumpStrength = 7;
		wallJumpStrength_horizontal = 7;
		wallJumpStrength_vertical = 7;
		maxShadowsOut = 1;
	}

	// Use this for initialization
	protected override void Start () {
		base.Start ();
	}

	protected override void FixedUpdate(){
		base.FixedUpdate ();
		if (recording) {
			shadowPath.AddPos(transform.position, GetComponent<SpriteRenderer>().sprite, currentSpecialAction);
		}
		currentSpecialAction = ShadowPath.SpecialAction.NOTHING;
	}

	// Update is called once per frame
	void Update () {
		
		if (isControllable) {
			bool isTouchingGround = IsTouchingGround ();
			int isTouchingWall = IsTouchingWall ();
			//animation
			if (animator.GetInteger ("AnimationState") != 3) {
				if (!isTouchingGround && isTouchingWall != 0) {
					animator.SetInteger ("AnimationState", 2);
					if (isTouchingWall == 1) {
						transform.localScale = new Vector3 (Mathf.Abs (transform.localScale.x), transform.localScale.y, transform.localScale.z);
					} else {
						transform.localScale = new Vector3 (-Mathf.Abs (transform.localScale.x), transform.localScale.y, transform.localScale.z);
					}
				} else if (Mathf.Abs (rigidBody.velocity.x) > MAX_STANDING_SPEED) {
					animator.SetInteger ("AnimationState", 1);
					if (rigidBody.velocity.x < 0) {
						transform.localScale = new Vector3 (-Mathf.Abs (transform.localScale.x), transform.localScale.y, transform.localScale.z);
					} else {
						transform.localScale = new Vector3 (Mathf.Abs (transform.localScale.x), transform.localScale.y, transform.localScale.z);
						//print(transform.localScale);
					}
				} else {
					animator.SetInteger ("AnimationState", 0);
				}
			}

		//control
			float dir = Input.GetAxisRaw ("Horizontal");
			if (dir == 0) {
				if (isTouchingGround) {
					if (Mathf.Abs (rigidBody.velocity.x) < deccel) {
						rigidBody.velocity = new Vector2 (0, rigidBody.velocity.y);
					} else {
						float myDir = rigidBody.velocity.x < 0 ? -1 : 1;
						rigidBody.velocity -= new Vector2 (myDir * deccel, 0);
					}
				}
			} else {
				if ((dir < 0 != rigidBody.velocity.x < 0) || Mathf.Abs (rigidBody.velocity.x) < speed) {
					float dif = 0;
					if (isTouchingGround) {
						dif = Mathf.Abs (dir * speed - rigidBody.velocity.x);
						rigidBody.velocity += new Vector2 (dir * Mathf.Min (accel, dif), 0);
					} else {
						dif = Mathf.Abs (dir * airSpeed - rigidBody.velocity.x);
						rigidBody.velocity += new Vector2 (dir * Mathf.Min (airAccel, dif), 0);
					}
				}
			
			}
			if (Input.GetKeyDown (KeyCode.Space)) {
				if (IsTouchingGround ()) {
					rigidBody.velocity += new Vector2 (0, jumpStrength);
				} else {
					if (isTouchingWall == 1) {
						rigidBody.velocity = new Vector2 (wallJumpStrength_horizontal, wallJumpStrength_vertical);
					} else if (isTouchingWall == 2) {
						rigidBody.velocity = new Vector2 (-wallJumpStrength_horizontal, wallJumpStrength_vertical);
					}
				}
			}

			if(Input.GetKeyDown(KeyCode.LeftControl)){
				if(recording){
					GameController.G.Rewind();
					recording = false;
					lastMarker.GetComponent<MarkerController>().stoppedTouching = false;
					lastMarker.GetComponent<SpriteRenderer>().color = Color.blue;
				}
			}

			if (Input.GetKeyDown (KeyCode.LeftShift)) {
				if (shadowPath != null && !recording && GameController.G.shadowsOut < maxShadowsOut) {
					GameObject shadow = Instantiate (GameController.G.Assets.guy1shadow);
					shadow.GetComponent<ShadowController> ().GivePath (shadowPath);
					shadowPath = shadowPath.Clone();
				}
			}

			if(Input.GetKeyDown(KeyCode.E)){
				if(GameController.G.PullLeverNear(transform.position, INTERACT_DISTANCE)){
					currentSpecialAction = ShadowPath.SpecialAction.PULL_LEVER;
					//animator.SetTrigger("ToPulling");
				}
			}

			if(Input.GetKeyDown(KeyCode.R)){
				Kill ();//for when you are trapped
			}

		}//if(isControllable)

	}

	public void StartMakingShadowPath(){
		shadowPath = new ShadowPath ();
		recording = true;
	}

	//protected override void LoadOriginalState(){
	//	base.LoadOriginalState ();
	//}

	public override void DisableBehavior ()
	{
		base.DisableBehavior ();
		isControllable = false;
	}

	public override void EnableBehavior(){
		base.EnableBehavior ();
		isControllable = true;
	}

	void OnCollisionEnter2D(Collision2D other){
		
	}

	void OnCollisionExit2D(Collision2D other){
		
	}

	public void Kill(){
		if (isControllable) {
			isControllable = false;
			animator.SetInteger("AnimationState", 3);
			StartCoroutine (KillCoroutine() );
		}
	}

	private IEnumerator KillCoroutine(){
		yield return new WaitForSeconds (1f);//time for death animation
		animator.SetInteger ("AnimationState", 0);
		GameController.G.Rewind ();

	}

	bool IsTouchingGround(){
		RaycastHit2D cast1 = Physics2D.Raycast (new Vector2(transform.position.x - 0.3f, transform.position.y - 0.9f), Vector2.down);
		RaycastHit2D cast2 = Physics2D.Raycast (new Vector2(transform.position.x + 0.3f, transform.position.y - 0.9f), Vector2.down);
		return (cast1.collider != null && !cast1.collider.isTrigger && cast1.distance < 0.1) || (cast2.collider != null && !cast2.collider.isTrigger && cast2.distance < 0.1);
	}

	int IsTouchingWall(){
		RaycastHit2D castLeft = Physics2D.Raycast (new Vector2(transform.position.x - 0.6f, transform.position.y), Vector2.left);
		RaycastHit2D castRight = Physics2D.Raycast (new Vector2(transform.position.x + 0.6f, transform.position.y), Vector2.right);
		bool rightWall = castRight.collider != null && !castRight.collider.isTrigger && castRight.distance < 0.1f;
		bool leftWall = castLeft.collider != null && !castLeft.collider.isTrigger && castLeft.distance < 0.1f;
		if (leftWall && rightWall) {
			return 3;
		}
		if (leftWall) {
			return 1;
		}
		if (rightWall) {
			return 2;
		}
		return 0;
	}

}
