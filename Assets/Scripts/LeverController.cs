﻿using UnityEngine;
using System.Collections;

public class LeverController : Rewindable {

	public const float DELAY = 0.8f;
	public GameObject[] reactors;
	public string[] scriptNames;
	private bool beingPulled;
	private Coroutine runningCoroutine;

	private bool savedBeingPulled;

	// Use this for initialization
	protected override void Start () {
		beingPulled = false;
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Pull(){
		if (!beingPulled) {
			runningCoroutine = StartCoroutine (PullHelper ());
		}
	}

	//rewind stuff
	private IEnumerator PullHelper(){
		beingPulled = true;
		GetComponent<Animator> ().SetTrigger ("Pull");
		yield return new WaitForSeconds(DELAY);
		for (int i=0; i<reactors.Length; i++) {
			((Reactor)reactors[i].GetComponent(scriptNames[i])).React();
		}
		beingPulled = false;
	}

	public override void DisableBehavior(){
		if (runningCoroutine != null) {
			StopCoroutine (runningCoroutine);
		}
		base.DisableBehavior();
	}
	
	protected override void SaveOriginalState(){
		savedBeingPulled = beingPulled;
		base.SaveOriginalState ();
	}
	
	protected override void LoadOriginalState(){
		beingPulled = savedBeingPulled;
		if (beingPulled) {
			Pull ();
		}
		base.LoadOriginalState ();
	}

}
