﻿using UnityEngine;
using System.Collections;

public class ShadowController : MonoBehaviour {

	private ShadowPath path;

	void Awake(){
		GameController.G.shadowsOut++;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		if (path != null) {
			if(path.IsDone()){
				Destroy(gameObject);
			}else{
				transform.position = path.NextVector ();
				GetComponent<SpriteRenderer>().sprite = path.NextSprite();
				switch(path.NextSpecialAction() ){
				case ShadowPath.SpecialAction.PULL_LEVER:
					if(!GameController.G.PullLeverNear(path.NextVector(), PlayerController.INTERACT_DISTANCE)){
						Debug.Log ("No lever close enough to pull");
					}
					break;
				default:

					break;
				}
				path.Increment ();
			}
		}

	}

	void OnDestroy(){
		GameController.G.shadowsOut--;
	}

	public void GivePath(ShadowPath path){
		this.path = path;
		transform.position = path.NextVector ();
	}
	
}
