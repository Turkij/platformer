﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rewindable : MonoBehaviour {

	public const int RECORDING_FRAMES_DENSITY = 10;
	private int recordingIndex;
	private int rewindIndex;
	private List<Sprite> recordedSprites;
	private List<Vector2> recordedPositions;
	private bool wasStartedLate;

	// Use this for initialization
	protected virtual void Start () {
		GameController.G.RegisterRewindable (this);
		recordingIndex = -1;
		rewindIndex = -1;
		recordedSprites = new List<Sprite> ();
		recordedPositions = new List<Vector2> ();
		wasStartedLate = false;
		if (GameController.G.IsRecording ()) {
			StartRecordingLate();
		}
	}

	protected virtual void FixedUpdate(){
		if (recordingIndex != -1) {
			if(recordingIndex % RECORDING_FRAMES_DENSITY == 0){
				RecordState ();
			}
			recordingIndex++;
		} else if (rewindIndex != -1) {
			LoadRecordedState (rewindIndex);
			if (rewindIndex == 0) {
				LoadOriginalState();
			}
			rewindIndex--;
		}
	}
	
	public void StartRecording(){
		recordingIndex = 0;
		wasStartedLate = false;
		SaveOriginalState ();
	}

	public void StartRecordingLate(){//this is for objects that are created while a recording is in progress
		recordingIndex = 0;
		wasStartedLate = true;
	}
	
	public void StartRewind(){
		recordingIndex = -1;
		rewindIndex = recordedSprites.Count-1;
		DisableBehavior ();
	}

	public virtual void Remove(){//this method should be used instead of Destroy for Rewindable GameObjects
		if (recordingIndex == -1) {
			Destroy (this);
		} else {
			DisableBehavior();
			Hide ();
		}
	}

	protected virtual void RecordState(){
		recordedSprites.Add (GetComponent<SpriteRenderer> ().sprite);
		recordedPositions.Add (transform.position);
	}

	protected virtual void LoadRecordedState(int index){
		transform.position = recordedPositions [index];
		GetComponent<SpriteRenderer> ().sprite = recordedSprites [index];
	}
	
	public virtual void DisableBehavior(){
		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		if(rb != null){
			rb.simulated = false;
		}
		Animator anim = GetComponent<Animator> ();
		if (anim != null) {
			anim.enabled = false;
		}
	}
	
	public virtual void EnableBehavior(){
		Rigidbody2D rb = GetComponent<Rigidbody2D>();
		if(rb != null){
			rb.simulated = true;
		}
		Animator anim = GetComponent<Animator> ();
		if (anim != null) {
			anim.enabled = true;
		}
	}
	
	public virtual void Hide(){
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		if(sr != null){
			sr.enabled = false;
		}
	}
	
	public virtual void UnHide(){
		SpriteRenderer sr = GetComponent<SpriteRenderer>();
		if(sr != null){
			sr.enabled = true;
		}
	}

	protected virtual void LoadOriginalState(){
		if (wasStartedLate) {
			Destroy (gameObject);
		} else {
			EnableBehavior ();
			ClearRecordings();
		}
	}

	public virtual void ClearRecordings(){
		recordedSprites.Clear ();
		recordedPositions.Clear ();
	}

	protected virtual void SaveOriginalState(){
		//add additional state information
	} 
}
