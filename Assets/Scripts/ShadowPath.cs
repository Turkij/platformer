﻿using UnityEngine;
using System.Collections.Generic;

public class ShadowPath {
	public enum SpecialAction {NOTHING, PULL_LEVER};

	private List<Vector2> positions;
	private List<Sprite> sprites;
	private List<SpecialAction> specialActions;
	private int index;

	public ShadowPath(){
		positions = new List<Vector2> ();
		sprites = new List<Sprite>();
		specialActions = new List<SpecialAction>();
		index = 0;
	}

	public void AddPos(Vector2 vector, Sprite sprite){
		AddPos (vector, sprite, SpecialAction.NOTHING);
	}

	public void AddPos(Vector2 vector, Sprite sprite, SpecialAction specialAction){
		positions.Add (vector);
		sprites.Add (sprite);
		specialActions.Add (specialAction);
	}

	public bool IsDone(){
		return index == Length ();
	}

	public Vector2 NextVector(){
		return positions [index];
	}

	public Sprite NextSprite(){
		return sprites [index];
	}

	public SpecialAction NextSpecialAction(){
		return specialActions [index];
	}

	public void Increment(){
		++index;
	}

	public void Decrement(){
		--index;
	}

	public int Length(){
		return positions.Count;
	}

	public ShadowPath Clone(){
		ShadowPath result = new ShadowPath ();
		for (int i=0; i<positions.Count; i++) {
			result.positions.Add (positions[i]);
			result.sprites.Add (sprites[i]);
			result.specialActions.Add (specialActions[i]);
		}
		return result;
	}
}
