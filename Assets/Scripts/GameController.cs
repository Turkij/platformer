﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public static GameController G;
	public Assets Assets;
	public int shadowsOut;
	public GameObject player;

	private List<Rewindable> rewindables;
	public int recordingIndex;
	public int rewindingIndex;

	void Awake(){
		rewindables = new List<Rewindable> ();
		recordingIndex = -1;
		rewindingIndex = -1;
		G = this;
		shadowsOut = 0;
	}

	// Use this for initialization
	void Start () {
		StartCoroutine (StartHelper ());//waits one frame so that everything can register
	}

	private IEnumerator StartHelper(){
		yield return null;
		StartRecording ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		if (recordingIndex != -1) {
			recordingIndex++;
		}
		if (rewindingIndex == 0) {
			StartRecording();
		}
		if (rewindingIndex != -1) {
			rewindingIndex--;
		} else {
			//StartRecording();
		}
	}

	public bool IsRecording(){
		return recordingIndex != -1;
	}

	public bool IsRewinding(){
		return rewindingIndex != -1;
	}

	public void RegisterRewindable(Rewindable rewindable){
		rewindables.Add (rewindable);
		if (IsRecording ()) {
			rewindable.StartRecordingLate();
		}
	}

	public void StartRecording(){
		recordingIndex = 0;
		foreach (Rewindable r in rewindables) {
			if(r != null){
				r.StartRecording();
			}
		}
	}

	public void Rewind(){
		rewindingIndex = recordingIndex / 10 + 1;
		recordingIndex = -1;
		foreach (Rewindable r in rewindables) {
			r.StartRewind();
		}
	}

	public void DiscardRecording(){
		rewindingIndex = 0;
		recordingIndex = -1;
		foreach (Rewindable r in rewindables) {
			r.ClearRecordings();
		}
	}

	public bool PullLeverNear(Vector3 pos, float dist){
		GameObject[] tagged = GameObject.FindGameObjectsWithTag("Lever");
		int i=0;
		while(i < tagged.Length && Vector3.Distance(tagged[i].transform.position, pos) > dist){
			i++;
		}
		if(i < tagged.Length){
			//animation stuff
			//set position
			LeverController lc = tagged[i].GetComponent<LeverController>();
			lc.Pull();
		}
		return i < tagged.Length;
	}

}
