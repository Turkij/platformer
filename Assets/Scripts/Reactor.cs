﻿using UnityEngine;
using System.Collections;

public abstract class Reactor : Rewindable {
	public abstract void React();
}
