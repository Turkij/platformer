﻿using UnityEngine;
using System.Collections;

public class MarkerController : MonoBehaviour {

	public bool stoppedTouching;

	// Use this for initialization
	void Start () {
		stoppedTouching = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){
		if (stoppedTouching && col.name == "Player") {
			stoppedTouching = false;
			col.transform.position = transform.position;
			col.GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
			PlayerController pc = col.GetComponent<PlayerController> ();
			if (pc.lastMarker != null) {
				pc.lastMarker.GetComponent<SpriteRenderer> ().color = Color.white;
			}
			pc.lastMarker = gameObject;
			GameController.G.DiscardRecording ();
			col.GetComponent<PlayerController> ().StartMakingShadowPath ();
			GetComponent<SpriteRenderer> ().color = Color.red + Color.blue;
		} else {
			GameController.G.DiscardRecording ();
		}
	}

	void OnTriggerExit2D(Collider2D col){
		if (col.name == "Player") {
			stoppedTouching = true;
		}
	}
}
