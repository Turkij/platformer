﻿using UnityEngine;
using System.Collections;

public class KillOnTouch : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.name == "Player") {
			other.gameObject.GetComponent<PlayerController>().Kill ();
		}
	}
}
